const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	}
];

const rootDiv = document.createElement('div');
rootDiv.id = 'root';

const listContainer = document.createElement('ul');

books.forEach(book => {
	try {
		if (!book.hasOwnProperty('author') || !book.hasOwnProperty('name') || !book.hasOwnProperty('price')) {
			console.error(`Something went wrong: Missing properties - ${!book.hasOwnProperty('author') ? 'author ' : ''}${!book.hasOwnProperty('name') ? 'name ' : ''}${!book.hasOwnProperty('price') ? 'price' : ''}`);
			return;
		}
	
		const listItem = document.createElement('li');
		const bookInfo = document.createElement('span');
	
		bookInfo.textContent = `${book.author}: ${book.name}`;
	
		if (book.price) {
			const priceInfo = document.createElement('span');
			priceInfo.textContent = ` - Price: ${book.price}`;
			bookInfo.appendChild(priceInfo);
		}
	
		listItem.appendChild(bookInfo);
		listContainer.appendChild(listItem);
	} catch (error) {
		console.log(error);
	}
});

rootDiv.appendChild(listContainer);
document.body.appendChild(rootDiv);